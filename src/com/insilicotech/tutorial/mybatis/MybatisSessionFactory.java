package com.insilicotech.tutorial.mybatis;

import java.io.*;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.*;

public class MybatisSessionFactory {

	private static SqlSessionFactory sqlSessionFactory = null;
	
	public static SqlSession getSession() {
		return getSqlSessionFactory().openSession();
	}
	
	private static SqlSessionFactory getSqlSessionFactory() {
		if (sqlSessionFactory == null ) {
			InputStream is;
			
			try {
				is = Resources.getResourceAsStream("com/insilicotech/tutorial/mybatis/mybatis-config.xml");
				sqlSessionFactory = new SqlSessionFactoryBuilder().build(is);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return sqlSessionFactory;
	}
	
	
}
