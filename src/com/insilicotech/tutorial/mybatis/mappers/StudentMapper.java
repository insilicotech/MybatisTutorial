package com.insilicotech.tutorial.mybatis.mappers;

import java.util.List;
import com.insilicotech.tutorial.mybatis.domain.Student;

public interface StudentMapper {
	List<Student> 	findAllStudents	();
	Student			findStudentById	(Integer id);
	void			insertStudent	(Student student);
}
