package com.insilicotech.tutorial.mybatis;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TableStudent {
	private static final String SQL_CREATE = 
		"CREATE TABLE IF NOT EXISTS STUDENTS ("
		+ "stud_id int(11) NOT NULL AUTO_INCREMENT"
  		+ ", name varchar(50) NOT NULL"
  		+ ", email varchar(50) NOT NULL"
  		+ ", dob date DEFAULT NULL"
  		+ ", PRIMARY KEY (stud_id));";
	
	private static final String SQL_DELETE = 
		"DELETE FROM STUDENTS;";
	
	private static final String SQL_QUERY = 
		"SELECT * FROM STUDENTS;";
	
	private static final String [] DML = {
		"insert  into students(stud_id,name,email,dob) values (1,'Student1','student1@gmail.com','1983-06-25');",
		"insert  into students(stud_id,name,email,dob) values (2,'Student2','student2@gmail.“com','198-06-25');"
	};

	public static void createTable() {
		Connection con = H2Database.getDBConnection();
		Statement  stmt = null;
		
		try {
			con.setAutoCommit(true);
			stmt = con.createStatement();
			stmt.execute(SQL_CREATE);
			stmt.execute(SQL_DELETE);
			for (int i=0; i<DML.length; i++) {
				stmt.execute(DML[i]);
			}
			
			ResultSet rs = stmt.executeQuery(SQL_QUERY);
			System.out.println("H2 Database inserted through Statement");
            while (rs.next()) {
                System.out.println("Id="+rs.getInt("stud_id")+" Name="+rs.getString("name"));
            }
            stmt.close();
            con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
