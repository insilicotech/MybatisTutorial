package com.insilicotech.tutorial.mybatis;

import java.util.List;

import org.h2.*;
import com.insilicotech.tutorial.mybatis.dao.StudentMybatisDaoImpl;
import com.insilicotech.tutorial.mybatis.domain.Student;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//H2Server.start();
		//H2Server.stop();
		
		//H2Database.getDBConnection();
		TableStudent.createTable();
		
		MybatisSessionFactory.getSession();
		StudentMybatisDaoImpl dao = new StudentMybatisDaoImpl();
		List<Student>	students = dao.findAllStudents();
		
		for (Student student : students) {
			System.out.println(student.toString());
		}
	}

}
