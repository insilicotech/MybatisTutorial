package com.insilicotech.tutorial.mybatis.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2 {
	  private static final String DB_DRIVER = "org.h2.Driver";
	  private static final String DB_CONNECTION = "jdbc:h2:~/test";
	  private static final String DB_USER = "sa";
	  private static final String DB_PASSWORD = "";
	  
	  /////////////////////////////////////////////////////////////////////////
	  //
	  //		<< Implementation >>
	  //
	  /////////////////////////////////////////////////////////////////////////
	  public static Connection getDBConnection() {
		  Connection connection = null;
		  
		  try {
			  Class.forName(DB_DRIVER);
		  } catch (ClassNotFoundException e) {
			  System.out.println(e.getMessage());
		  }
		  
		  try {
			  connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
			  return connection;
		  } catch (SQLException e) {
			  System.out.println(e.getMessage());
		  }
		  
		  return connection;
		  
	  }

}
