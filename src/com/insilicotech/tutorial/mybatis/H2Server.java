package com.insilicotech.tutorial.mybatis;
import java.sql.SQLException;

import org.h2.tools.Server;

public class H2Server {

	private static Server server = null;
	static {
		try {
			server = Server.createTcpServer(new String[]{"-tcp","-tcpAllowOthers","-tcpDaemon"});
		} catch (SQLException e) {
			
		}
	}
	
	public static void start() {
		if ( server !=null && !server.isRunning(false) ) {
			try {
				server.start();
			} catch (SQLException e) {
				
			}
			
		}
	}
	
	public static void stop() {
		if (server !=null ) {
			server.stop();
		}
	}
	
	/*
	private static void initServer() {
		try {
			server = Server.createTcpServer(new String[]{"-tcp","-tcpAllowOthers","-tcpDaemon"});
		} catch (SQLException e) {
			
		}
	}
	*/
}
