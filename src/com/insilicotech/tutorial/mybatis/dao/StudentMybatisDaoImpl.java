package com.insilicotech.tutorial.mybatis.dao;

import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.insilicotech.tutorial.mybatis.domain.Student;
import com.insilicotech.tutorial.mybatis.mappers.StudentMapper;
import com.insilicotech.tutorial.mybatis.MybatisSessionFactory; 

public class StudentMybatisDaoImpl {
	  private Logger logger = LoggerFactory.getLogger(getClass());
	  
	  public List<Student> findAllStudents()
	  {
	    SqlSession sqlSession = MybatisSessionFactory.getSession();
	      try {
	      StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);
	      return studentMapper.findAllStudents();
	    } finally {
	    	//If sqlSession is not closed 
	    	//then database Connection associated this sqlSession will not be returned to pool 
	    	//and application may run out of connections.
	    	sqlSession.close();
	    }
	  }
}
