package com.insilicotech.tutorial.mybatis.domain;

import java.util.Date;

public class Student {

	private Integer studId;
	private String	name;
	private String 	email;
	private Date	dob;
	
	public Integer 	getStudId() 	{ return studId; }
	public String	getName() 		{ return name; }
	public String	getEmail() 		{ return email; }
	public Date		getDob()		{ return dob; }
	
	public void setStudId(Integer id) 	{ this.studId = id; }
	public void setName(String name ) 	{ this.name = name; }
	public void setEmail(String email) 	{ this.email = email; }
	public void setDob(Date dob) 		{ this.dob = dob; }
	
	@Override
	public String toString() {
		return "id=" + getStudId() 
				+ ", name=" + getName() 
				+ ", email=" + getEmail() 
				+ ", date=" + getDob();
	}
}
